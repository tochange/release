{
	"user": "all",
	"apps": {
		"Fui": {
			"Android": [{
					"version_name": "2.639beta",
					"version_code": 155,
					"download_path": ["https://imtt.dd.qq.com/16891/apk/990458E789CFE978F81A1CA7AEF32E4C.apk?fsname=com.joey.fui_2.639beta_155.apk&csr=1bbd"],
					"feature": [
						"新增海报功能",
						"丰富可撤销内容",
						"支持保存成正方形",
						"动态和资料可上传语音",
						"印章增加名人篆刻款识",
						"贴纸支持自己创建二维码"
					]
				}, {
					"state": -1,
					"version_name": "2.622",
					"version_code": 124,
					"download_path": ["https://imtt.dd.qq.com/16891/apk/FB212C96D4429E394D45E9AF201F8EBA.apk?fsname=com.joey.fui_2.622_124.apk&csr=1bbd"],
					"release_note": [{
						"id": "00112401",
						"type": 1,
						"describe": "增加41张场景图"
					}, {
						"id": "00112402",
						"type": 1,
						"describe": "增加撤销、重做功能"
					}, {
						"id": "00112403",
						"type": 1,
						"describe": "场景图自动调节位置"
					}, {
						"id": "00112404",
						"type": 2,
						"describe": "修复小米老要评论问题"
					}, {
						"id": "00112405",
						"type": 2,
						"describe": "修复批量加框位置错乱问题"
					}]
				}, {
					"state": -1,
					"version_name": "2.619",
					"version_code": 121,
					"download_path": ["https://fe493dce0c38b42121e04741c9e75370.dd.cdntips.com/imtt.dd.qq.com/16891/apk/00169AF118FF402F2A6EBAD8676C5E63.apk?mkey=5ed49b6175adc514&f=8ea4&fsname=com.joey.fui_2.619_121.apk&csr=1bbd"],
					"release_note": [{
						"id": "00112101",
						"type": 1,
						"describe": "增加刻章功能"
					}]
				}, {
					"state": -1,
					"version_name": "2.613delta",
					"version_code": 102,
					"download_path": ["https://imtt.dd.qq.com/16891/apk/7A1AB6FC074237F4EE9189C4146422DB.apk?fsname=com.joey.fui_2.613delta_102.apk&csr=1bbd"],
					"release_note": [{
						"id": "00110201",
						"type": 1,
						"describe": "增加调色板"
					}, {
						"id": "00110202",
						"type": 1,
						"describe": "自定义背景图放到较前"
					}, {
						"id": "00110203",
						"type": 1,
						"describe": "增加获赞显示"
					}]
				}, {
					"state": -1,
					"version_name": "",
					"version_code": 101,
					"download_path": [""],
					"release_note": []
				}, {
					"state": -1,
					"version_name": "",
					"version_code": 100,
					"download_path": [""],
					"release_note": []
				},
				{
					"state": -1,
					"version_name": "",
					"version_code": 99,
					"download_path": [""],
					"release_note": []
				},
				{
					"state": -1,
					"version_name": "",
					"version_code": 98,
					"download_path": [""],
					"release_note": []
				},

				{
					"state": -1,
					"version_name": "",
					"version_code": 97,
					"download_path": [""],
					"release_note": []
				},

				{
					"state": -1,
					"version_name": "",
					"version_code": 96,
					"download_path": [""],
					"release_note": []
				},

				{
					"state": -1,
					"version_name": "2.601gamma",
					"version_code": 87,
					"download_path": ["https://imtt.dd.qq.com/16891/apk/F1CFCE34EFD92DEFDC56F2C4C7FF81C3.apk?fsname=com.joey.fui_2.601gamma_87.apk&csr=1bbd"],
					"release_note": [{
						"id": "00108201",
						"type": 1,
						"describe": "支持上传图片"
					}]
				}, {
					"state": -1,
					"version_name": "2.514delte",
					"version_code": 59,
					"download_path": ["https://imtt.dd.qq.com/16891/apk/87BE2CDF6E20A89D0D603F4B22EB3C83.apk?fsname=com.joey.fui_2.514delte_59.apk&csr=1bbd"],
					"release_note": [{
						"id": "00105801",
						"type": 1,
						"describe": "支持一天会员充值"
					}, {
						"id": "00105802",
						"type": 1,
						"describe": "支持单张旋转"
					}, {
						"id": "00105803",
						"type": 1,
						"describe": "增加三种相框和若干场景图"
					}, {
						"id": "00105804",
						"type": 1,
						"describe": "优化交互操作"
					}]
				}
			],
			"iOS": [{
				"version_name": "",
				"version_code": 11,
				"state": 1,
				"download_path": ["", ""],
				"release_note": [{
					"id": "",
					"type": 1,
					"describe": ""
				}]
			}]
		}
	}
}